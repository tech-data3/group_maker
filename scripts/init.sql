DROP TABLE IF EXISTS apprenant;
CREATE TABLE apprenant(
    apprenant_id INTEGER PRIMARY KEY,
    nom_complet VARCHAR(30) UNIQUE
);

DROP TABLE IF EXISTS apprenant_groupe_exo;
CREATE TABLE apprenant_groupe_exo(
    exo_id INTEGER NOT NULL,
    apprenant_id INTEGER NOT NULL REFERENCES apprenant(apprenant_id),
    group_id INTEGER NOT NULL,
    PRIMARY KEY(exo_id, apprenant_id, group_id)
);

INSERT INTO
    apprenant(nom_complet)
VALUES
    ('julien'),
    ('arsene'),
    ('angelo'),
    ('jacques'),
    ('jean'),
    ('benjamin'),
    ('karine'),
    ('narayan'),
    ('nicolas m.'),
    ('nicolas p.'),
    ('anthony'),
    ('lola'),
    ('isabelle');

SET @id_julien = (SELECT apprenant_id FROM appreneant WHERE nom_complet = 'arsene');
SET @id_narayan = SELECT apprenant_id FROM appreneant WHERE nom_complet = "narayan";
SET @id_jean = SELECT apprenant_id FROM appreneant WHERE nom_complet = "jean";
SET @id_isabelle = SELECT apprenant_id FROM appreneant WHERE nom_complet = "isabelle";
SET @id_lola = SELECT apprenant_id FROM appreneant WHERE nom_complet = "lola";
SET @id_jacques = SELECT apprenant_id FROM appreneant WHERE nom_complet = "jacques";
SET @id_nicolas_m = SELECT apprenant_id FROM appreneant WHERE nom_complet = "nicolas m";
INSERT INTO apprenant_groupe_exo
    (exo_id, group_id, apprenant_id)
VALUES
    (1,1,@id_julien),
    (1,1,@id_narayan),
    (1,1,@id_jean),
    (1,1,@id_isabelle),
    (1,2,@id_lola),
    (1,2,@id_jacques),
    (1,2,@id_nicolas_m),
    (1,3,@id_angelo),
    (1,3,@id_benjamin),
    (1,3,@id_julien),
    (2,4,@id_nicolas_p),
    (2,4,@id_anthony),
    (2,5,@id_narayan),
    (2,5,@id_isabelle),
    (2,6,@id_arsene),
    (2,6,@id_nicolas_m),
    (2,7,@id_julien),
    (2,7,@id_jean),
    (3,8,@id_lola),
    (3,8,@id_antony),
    (3,9,@id_nicolas_p),
    (3,9,@id_jacques),
    (4,10,@id_arsene),
    (4,10,@id_jacques),
    (4,10,@id_isabelle),
    (4,10,@id_karine),
    (4,11,@id_narayan),
    (4,11,@id_lola),
    (4,12,@id_jean),
    (4,12,@id_benjamin),
    (4,12,@id_nicolas_p),
    (4,12,@id_antony),
    (4,13,@id_angelo),
    (4,13,@id_nicolas_m),
    (4,13,@id_julien),
    (5, 14,@id_jean),
    (5, 14,@id_jacques),
    (5, 14,@id_julien),
    (5, 15,@id_narayan),
    (5, 15,@id_benjamin),
    (5, 15,@id_nicolas_p),
    (5, 16,@id_nicolas_m),
    (5, 16,@id_antony),
    (5, 16,@id_karine);

