import re 
import sqlite3
from pprint import pprint


def get_matrice_indice_from_list_indice(i):
    # on a n=1 m=2 comme indice 0 du tableau
    # n(n+1)/2-n est le dernier ellement du tableau
    # la resolution est beaucoup plus simple sur une matrice triangulaire
    m = (-1+int(math.sqrt(1 - 4 * 1 * -(i*2))))//2
    n = i - m*(m+1)//2
    return n, m


def get_list_indice_from_matrice_indice(n: int, m: int) -> int:
    return m * (m+1) // 2 + n


def get_data() -> list[list[int, int, int]]:
    with open("data/data", "r") as f:
        groups = []
        for l in f:
            pattern =r"(.*?)(\s{2,}|\n)"
            group = [nom.lower() for nom, space in re.findall(pattern, l)]
            groups.append(group)
        return groups


def pair_from_group(group: list[str])-> list[tuple[str,str]] :
    """
    liste les combinaison de 2 parmi n 

    groups: list of string
    return list of tuple of string
    """
    if not group:
        raise ValueError
    elif len(group) == 1:
        raise ValueError
    pair = []
    for index, person in enumerate(group):
        for person2 in group[index:]:
            pair.append((person,person2))
    return pair


def create_matrice(groups):
    person = {value: index for index, value in enumerate({nom for group in groups for nom in group})}
    len_matrice = len(person)*(len(person)+1)//2
    matrice = [0]*len_matrice
    for group in groups:
        for pair in pair_from_group(group):
            person1 = person[pair[0]]
            person2 = person[pair[1]]
            if (len(person)-0)*(person1-0)-(len(person)-0)*(person2-0)>0:
                person1, person2 = person2, person1
            matrice[get_list_indice_from_matrice_indice(person1, person2)]+=1
    return matrice, person


def print_matrice(matrice : list[int], person: dict):
    reverse_person = {v: k for k, v in person.items()}
    ligne = 0
    name_max_len = 0
    for i in range(len(reverse_person)):
        if len(reverse_person[i])> name_max_len: 
            name_max_len = len(reverse_person[i])
    header = "".join([f"{' '*(name_max_len+1)}"]+[f"{reverse_person[i]}{' '*(name_max_len-len(reverse_person[i])+1)}" for i in range(len(reverse_person))])

    print(header)
    print(f"{reverse_person[ligne]}{" "*(name_max_len-len(reverse_person[ligne])+1)}", end = "")

    for index, value in enumerate(matrice):
        print(f"{str(value)}{" "*name_max_len}", end="")
        if index + 1 == (ligne+1) * (ligne+2) // 2:
            print()
            ligne += 1
            if index+1 < len(matrice):
                print(f"{reverse_person[ligne]}{" "*(name_max_len-len(reverse_person[ligne])+1)}", end = "")


def main():
    con = sqlite3.connect('../data/group_maker.db')
    cur = con.cursor()
    get_data()
    cur.close()
    con.close()


if __name__ == '__main__':
    print_matrice(*create_matrice(get_data()))


