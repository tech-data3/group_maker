# tech-data group maker

## what's the point

make group based on least grouped people.

## spec

### MCD

```mermaid
classDiagram
groupe --> apprenant: est compose d' (s)
groupe --> exercice: font des
```

### MLD

```mermaid
erDiagram
apprenant{
    id INT PK "AUTO_INCREMENT"
    denomination VARCHAR(30) PK "NOT NULL UNIQUE"
}

groupe_appreanant{
    id_exo INT PK "NOT_NULL"
    id_apprenant INT PK,FK "NOT_NULL"
    id_group INT PK,FK "NOT_NULL"
}

apprenant ||--|{ groupe_appreanant: ""
```

